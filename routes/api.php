<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'LoginController@Login');
Route::middleware('auth:api')->post('/import-file', 'importFileController@importFile');
Route::middleware('auth:api')->get('/get-employee-record', 'employeeController@getEmployeeRecord');

Route::middleware('auth:api')->get('/get-employee-by-created-date/{id}', 'employeeController@getEmployeeByCreatedDate');



Route::middleware('auth:api')->post('/register-employee', 'employeeController@registerEmployee');

Route::middleware('auth:api')->get('/logout', 'employeeController@Logout');

