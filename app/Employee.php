<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
  protected $fillable=[
    'image','full_name','date_of_birth','gender','salary','designation','creared_by','creared_date'
  ];
}
