<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){  
        $trusted_domains = ["http://localhost:4200","http://localhost:8100"];
        if(isset($request->server()['HTTP_ORIGIN'])) {
            $origin = $request->server()['HTTP_ORIGIN'];
              if(in_array($origin, $trusted_domains)) {
                header('Access-Control-Allow-Origin: ' . $origin);
                header('Access-Control-Allow-Headers: Origin, Content-Type, x-requested-with, Accept, Authorization, Client-Security-Token, Accept-Encoding ,mimeType,X-CSRF-TOKEN,X-XSRF-TOKEN,xccm');
                header('Access-Control-Allow-Credentials: true');
                }
             }
            return $next($request);
        }
}
