<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Excel;

use Auth;

use App\Employee;

class importFileController extends Controller
{

    public function __construct(){

        $this->middleware('auth');
        
    }

    public function importFile(Request $request){

        $path = $request->file('import_file')->getRealPath();

        $employeeInfo = \Excel::load($path)->get();

        $emptyIndex=array();

        if($employeeInfo->count()){

            for ($i=0; $i < count($employeeInfo); $i++) { 

                $flag =0;

                if(!is_null($employeeInfo[$i]->salary) && !is_null($employeeInfo[$i]->gender)  && !is_null($employeeInfo[$i]->date) && !is_null($employeeInfo[$i]->designation) && !is_null($employeeInfo[$i]->full_name)){
                     
                    if(is_a($employeeInfo[$i]->date, 'DateTime')){


                         if(is_float($employeeInfo[$i]->salary)){
                             
                                $employeeLists[] = [
                                            'salary' => $employeeInfo[$i]->salary,
                                            'date_of_birth' => $employeeInfo[$i]->date,
                                            'gender'=>$employeeInfo[$i]->gender,
                                            'full_name'=>$employeeInfo[$i]->full_name,
                                            'creared_date'=>date('Y-m-d'),
                                            'creared_by'=> Auth::user()->name,
                                            'designation'=>$employeeInfo[$i]->designation
                                     ];

                         }else{

                            return response()->json(['res'=>'InValid data Type Of Salary','index'=>$i+1,'flag'=>0]);

                         }
                    } else{

                        return response()->json(['res'=>'InValid data Type Of Date','index'=>$i+1,'flag'=>0]);

                    }
                }else{

                    if(count($employeeInfo) != $i + 1){

                       if(is_null($employeeInfo[$i]->salary)){

                         if(is_null($employeeInfo[$i]->date)){

                             if(is_null($employeeInfo[$i]->designation)){

                                if(is_null($employeeInfo[$i]->full_name)){

                                    if(is_null($employeeInfo[$i]->gender)){
                                        array_push($emptyIndex,$i);
                                    } else{

                                        return response()->json(['res'=>'Full Name is does not exist in','index'=>$i+1,'flag'=>0]);

                                    }
                                }else{

                                    return response()->json(['res'=>'Designation is does not exist in','index'=>$i+1,'flag'=>0]);

                                }
                             }else{

                                return response()->json(['res'=>'Date is does not exist in','index'=>$i+1,'flag'=>0]);

                             }
                          }else{

                            return response()->json(['res'=>'Salary is does not exist in','index'=>$i+1,'flag'=>0]);

                          }
                       } else{

                        if(is_null($employeeInfo[$i]->date)){

                            return response()->json(['res'=>'Date is does not exist in','index'=>$i+1,'flag'=>0]);

                         }else{

                            if(is_null($employeeInfo[$i]->designation)){

                                return response()->json(['res'=>'Designation is does not exist in','index'=>$i+1,'flag'=>0]);

                             }else{

                                if(is_null($employeeInfo[$i]->full_name)){

                                    return response()->json(['res'=>'Full Name is does not exist in','index'=>$i+1,'flag'=>0]);
                                    
                                  }else{

                                    if(is_null($employeeInfo[$i]->gender)){

                                        return response()->json(['res'=>'Gender is does not exist in','index'=>$i+1,'flag'=>0]);

                                      }
                                  }
                               } 
                           }
                       }
                    }
                 }
              }


            if(!empty($employeeLists)){

                Employee::insert($employeeLists);

                if(count($emptyIndex) > 0){

                    return response()->json(['res'=>'sucessfully Inserted','index'=>implode(', ', $emptyIndex),'flag'=>1]);

                }else{

                    return response()->json(['res'=>'sucessfully Inserted','index'=>'','flag'=>0]);

                }
            }


        }else{

            return response()->json(['res'=>'No date available','index'=>'','flag'=>0]);

        }
    }
}
 