<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Auth;
use App\User;
use DB;
class employeeController extends Controller
{
    private $imageServer;


    public function __construct(){
        $this->imageServer = public_path();
        $this->middleware('auth');

    }

    public function getEmployeeRecord(){
        return response()->json([
            'employee'=>Employee::orderBy('id', 'desc')->paginate(10),
            'date'=>Employee::groupby('creared_date')->select('id','creared_date')->get()
        ]);
    }
    public function getEmployeeByCreatedDate($id){
       return response()->json([
          'employee'=>Employee::where('creared_date',$id)->paginate(10)
       ]);
    }
    public function registerEmployee(Request $request){


        if($request->is_edit == 'true'){

            $profileImage = $request->image;

            $data = json_decode($request->data);

            $filename;

            if ($profileImage) {

                $filename = uniqid() . $profileImage->getClientOriginalName();
                $filename = preg_replace('/\s+/', '_', $filename);
                $profileImage->move($this->imageServer . '/images/profile-pic', $filename);


                Employee::where('id',$data->id)->update([
                    'image'=>$filename,
                    'full_name'=>$data->full_name,
                    'date_of_birth'=>$data->date,
                    'gender'=>$data->gender,
                    'salary'=>$data->salary,
                    'designation'=>$data->designation
                ]);
                
            }else{

                Employee::where('id',$data->id)->update([
                    'full_name'=>$data->full_name,
                    'date_of_birth'=>$data->date,
                    'gender'=>$data->gender,
                    'salary'=>$data->salary,
                    'designation'=>$data->designation
                ]);


            }

            return response()->json(['res'=>'2']);


        }else{

            $profileImage = $request->image;

            $filename='';

            if ($profileImage) {

                $filename = uniqid() . $profileImage->getClientOriginalName();
                $filename = preg_replace('/\s+/', '_', $filename);
                $profileImage->move($this->imageServer . '/images/profile-pic', $filename);

                
            }

                $data = json_decode($request->data);

                Employee::create([
                    'image'=>$filename,
                    'full_name'=>$data->full_name,
                    'date_of_birth'=>$data->date,
                    'gender'=>$data->gender,
                    'salary'=>$data->salary,
                    'designation'=>$data->designation,
                    'creared_by'=>Auth::user()->name,
                    'creared_date'=>date('Y-m-d'),
                ]);
    
           return response()->json(['res'=>'1']);
  
        }
    }
   public function Logout(){
     DB::table('oauth_access_tokens')
        ->where('user_id', Auth::user()->id)
        ->update([
            'revoked' => true
        ]);
     return response()->json(['LogOut']);   
   }

}
