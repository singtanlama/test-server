<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function Login(Request $request){
        $http = new Client();
        $response = $http->post('http://Test.server.own' . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                // 'client_secret' => env('APP_CLIENT'),
                'client_secret' =>'ANjnikU5zmo3aEu81cVr2WXtKSBGUA8KsX08YTh8',
                'username' => $request->input("email"),
                'password' => $request->input("password"),
            ],
        ]);
        $id;

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) ? 'email' : 'email';
        if (Auth::once([$field => $request->input("email"), 'password' => $request->input("password")])) {
            $id = Auth::user()->id;
            $email = Auth::user()->email;
            $name = Auth::user()->name;
        }
        return ([
            "id"=>Auth::id(),
            "access_token" => json_decode((string)$response->getBody(), true)['access_token']
        ]);
    }
}
